class SessionsController < ApplicationController
  def new
  end

  def login
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    puts "session :>", params[:session]
    if @user && @user.password === params[:session][:password] && @user.email === params[:session][:email]
      log_in @user
      redirect_to @user
      # Log the user in and redirect to the user's show page.
    else
      # Create an error message.
      flash[:danger] = 'Invalid email/password combination' # Not quite right!
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
