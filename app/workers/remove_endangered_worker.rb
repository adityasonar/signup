class RemoveEndangeredWorker
    include Sidekiq::Worker
    sidekiq_options retry: false

    def perform
        User.destroy_all
    end
end
