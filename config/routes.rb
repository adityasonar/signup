Rails.application.routes.draw do
  get 'sessions/new'
  resources :users
  get  '/signup',  to: 'signup#signup'
  post '/signup', to: 'signup#signup'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'users#index'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  # get    '/welcome', to: 'welcome#show'
end
