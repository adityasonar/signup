class AddNameColumnUser < ActiveRecord::Migration[5.2]
  def change
    add_column :user, :name, :string
  end
end
